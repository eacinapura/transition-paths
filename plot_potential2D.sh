for i in `seq $1 $2`
do
    printf 'Running file '
    printf $i 
    printf ' \r'
    python3 -m analysis.plots.plot_potential2D data$i
done
printf '\n'