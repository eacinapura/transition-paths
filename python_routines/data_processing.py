# -*- coding: utf-8 -*-

import h5py
import numpy as np

def reduce(source_file, dest_file, factor):
    '''
        Saves the data from the source file, reduced to a specified factor

        Input
            source_file: path of the source file
            dest_file: path of the file where the data should be saved
            factor: what fraction of the original data should be saved
    '''
    with h5py.File(source_file, mode='r') as reader:
        x = np.array(reader['main/x'])
        y = np.array(reader['main/y'])
        z = np.array(reader['main/z'])
        pressure = reader['main'].attrs['pressure']
        data_ticks = reader['main'].attrs['data_ticks']

    x = x[:len(x)//factor]
    y = y[:len(y)//factor]
    z = z[:len(z)//factor]

    with h5py.File(dest_file, mode='w') as writer:
        grp = writer.create_group('main')
        grp.attrs['data_ticks'] = data_ticks
        grp.attrs['pressure'] = pressure
        grp.create_dataset('x', data=x)
        grp.create_dataset('y', data=y)
        grp.create_dataset('z', data=z)

def remove_DC(source_file, dest_file):
    '''
        Saves the data from the source file, subtracting the average of the signal

        Input
            source_file: path of the source file
            dest_file: path of the file where the data should be saved
    '''
    with h5py.File(source_file, mode='r') as reader:
        x = np.array(reader['main/x'])
        y = np.array(reader['main/y'])
        z = np.array(reader['main/z'])
        pressure = reader['main'].attrs['pressure']
        data_ticks = reader['main'].attrs['data_ticks']

    x = x - np.average(x)
    y = y - np.average(y)
    z = z - np.average(z)

    with h5py.File(dest_file, mode='w') as writer:
        grp = writer.create_group('main')
        grp.attrs['data_ticks'] = data_ticks
        grp.attrs['pressure'] = pressure
        grp.create_dataset('x', data=x)
        grp.create_dataset('y', data=y)
        grp.create_dataset('z', data=z)

def remove_oscillation(source_file, dest_file, L = 20000):
    '''Divide the signal in chunks and remove the average in each chunk
    ---------------------------
    Parameters:
        source_file: string
            path of the input file
        dest_file: string
            path of the output file
        L: integer
            length of the chunk in samples
    '''
    with h5py.File(source_file, mode='r') as reader:
        x = np.array(reader['main/x'])
        y = np.array(reader['main/y'])
        try:
            z = np.array(reader['main/z'])
        except:
            z = None
        pressure = reader['main'].attrs['pressure']
        try:
            data_ticks = reader['main'].attrs['data_ticks']
        except:
            pass
        try:
            fs = reader['main'].attrs['fs']
        except:
            pass


    N = len(x)

    # print(N//L)
    
    for i in range(N//L):
        mid_x = (max(x[i*L:(i+1)*L]) + min(x[i*L:(i+1)*L]))/2
        mid_y = (max(y[i*L:(i+1)*L]) + min(y[i*L:(i+1)*L]))/2
        x[i*L:(i+1)*L] = x[i*L:(i+1)*L] - mid_x
        y[i*L:(i+1)*L] = y[i*L:(i+1)*L] - mid_y
        if z != None:
            mid_z = (max(z[i*L:(i+1)*L]) + min(z[i*L:(i+1)*L]))/2
            z[i*L:(i+1)*L] = z[i*L:(i+1)*L] - mid_z

    x = np.delete(x, np.s_[(N//L)*L:])
    y = np.delete(y, np.s_[(N//L)*L:])
    if z != None:
        z = np.delete(z, np.s_[(N//L)*L:])

    with h5py.File(dest_file, mode='w') as writer:
        grp = writer.create_group('main')
        grp.create_dataset('x', data=x)
        grp.create_dataset('y', data=y)
        grp.create_dataset('z', data=z)
        grp.attrs['pressure'] = pressure
        try:
            grp.attrs['data_ticks'] = data_ticks
        except:
            pass
        try:
            grp.attrs['fs'] = fs
        except:
            pass