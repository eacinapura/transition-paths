import numpy as np
from scipy.optimize import curve_fit

def lorentzian(f, f0, gamma, A):
    return A / ((f**2 - f0**2)**2 + f**2 * gamma**2)

    
def estimate_parameters(freqs, amp):
    '''
        make an initial guess for the fitting parameters of the lorentzian to feed the fitting function

        Input: frequency and amplitude vectors of the window containing the peak and the FWHM
        Output: dict containing f0, gamma, A
    '''
    #===================================================
    # RESONANT FREQUENCY
    #===================================================
    # Estimate with the frequency for which amp is max
    #===================================================
    f0 = 0
    a_max = amp[0]
    i = -1
    i_max = 0
    for f, a in zip(freqs, amp):
        i += 1
        if a > a_max:
            a_max = a
            f0 = f
            i_max = i

    #===================================================
    # FWHM
    #===================================================
    # Estimate by finding left and right frequencies for
    # which amp = amp_max/2
    #===================================================
    eps = 0.1
    f_L = freqs[0]
    f_R = freqs[-1]
    for i in range(i_max, 0, -1):
        if a_max/2 - eps <= amp[i] <= a_max/2 + eps:
            f_L = freqs[i]
    for i in range(i_max, len(freqs)):
        if a_max/2 - eps <= amp[i] <= a_max/2 + eps:
            f_R = freqs[i]

    gamma = abs(f_R - f_L)
    #===================================================
    # A FACTOR
    #===================================================
    # Simply use math and invert formula at f = f0
    #===================================================
    A = a_max * (f0**2 * gamma**2)

    output = {'f0':f0, 'gamma':gamma, 'A':A}
    return output

def fit(freqs_in, data_in, low, high):
    '''
        Fit input data with a Lorentzian and return fitting parameters.

        Input:  array of frequencies and amplitudes
                lower and upper frequencies delimiting the window where the fitting must be done
        Output: dict containing f0, gamma, A and freqs (the frequency window used for the fit)
    '''
    #=========================
    # Make window of interest
    #=========================
    freqs = np.array([])
    data = np.array([])
    for f, d in zip(freqs_in, data_in):
        if low <= f <= high:
            freqs = np.append(freqs, f)
            data = np.append(data, d)
    
    #=========================
    # Fit with a lorentzian
    #=========================
    guesses = estimate_parameters(freqs, data)
    guess = [guesses['f0'], guesses['gamma'], guesses['A']]
    fit_o = curve_fit(lorentzian, freqs, data, guess)
    f0, gamma, amp = fit_o[0]
    fit_freqs = np.linspace(freqs[0], freqs[-1], 10000)

    return {'f0':f0, 'gamma':abs(gamma), 'amp':amp, 'freqs':fit_freqs}


