import numpy as np
from optomechanics.post_processing import spectrum, measurement
from scipy import constants
import numba

class LinearCalibrator():
    def __init__(self):
        self.parameters = {'is_calibrated'  : False,
                           'c_calibrated'   : None,      # bits/m
                           'particle_size'  : None,      # radius
                           'particle_mass'  : None,      # kg
                           'pressure'       : None,      # mbar
                           'temperature'    : None}      # K
    
    def calibrate(self,
                  trace, 
                  fs,
                  particle_size,
                  temperature = (297.5, 1),
                  freq_resolution = 100,
                  cut_to_power2 = True,
                  window = None,
                  nm = False):
        """
        Extracts calibration factor from the power spectral density of a trace assuming 1/2 m <v*^2> = 1/2 k_B T,
        where <v^2> is calculated from the psd of the given trace.
        -----------
        Parameters:
            trace: numpy.ndarray
                trace used to estimate the calibration parameters
            fs: float
                sampling frequency of trace
            particle_size: float
                radius of the particle in meters.
                Used to estimate the mass. 
            temperature: tuple of floats, optional
                temperature at which the timetrace has been recorded.
                temperature[0] is the estimated value, temperatur[1] is the error bar.
            window: list of two integers, optional
                interval of frequencies to conside in the psd
                    Example: if fit is desired from 30kHz to 50kHz then
                        window = [3e5,5e5]
            freq_resolution: float, optional
                when estimating the power spectral density, frequency resolution
                required in Hz. Defaults to 100
            cut_to_power2: bool, optional
                The required frequency resolution fixes the number of points per segment
                in the Welch estimation of the power spectral density. It is computationally
                advantageous, however, to have the number of points given by a power of two.
                It cut_to_power2 is True, then the segment size is adjusted accordingly.
                Defaults to False.
        """          
        self.parameters['is_calibrated'] = False

        if type(temperature) is not tuple:
            temperature = (temperature, 0)

        rho_SiO2 = 2200
        particle_mass = 4 * np.pi * rho_SiO2 * particle_size ** 3 / 3

        self.parameters['particle_size'] = particle_size
        self.parameters['particle_mass'] = particle_mass
        self.parameters['temperature'] = temperature

        N = len(trace)
        T = N/fs
        
        subdivision_factor = int(freq_resolution/(1/T))
        if subdivision_factor==0:
            subdivision_factor = 1

        psd, frequency = spectrum.derive_psd(trace,
                                             fs,
                                             subdivision_factor = subdivision_factor,
                                             cut_to_power2 = cut_to_power2)
        
        if window is not None:
            ind0,ind1 = measurement.timetrace.ext_indexes(frequency,window[0],window[1])
            psd = psd[ind0:ind1]
            frequency = frequency[ind0:ind1]

        # Actual calibration
        df = frequency[1]-frequency[0]
        integral = sum(psd * (frequency**2))*df
        c = np.sqrt(constants.Boltzmann * temperature[0] / (particle_mass * integral*4*np.pi**2))

        self.parameters['is_calibrated'] = True
        self.parameters['c_calibrated'] = c
        
    def calibrate_trace(self, trace, nm = False):
        '''
        Given a trace in bits (or Volts) the method returns the trace
        in meters.
        ------------------------
        Parameters:
            trace: numpy.ndarray 
                the timetrace to be calibrated. It needs to be a 
                numpy array such that division by scalar is possible.
            nm: bool, optional
                whether to return the trace in nanometers or in meters.
                Defaults to True.
        -----------------------
        Returns:
            transformed trace
        -----------------------
        '''
        if not self.parameters['is_calibrated']:
            raise Exception('Calibrator not initialized yet.')
        c = self.parameters['c_calibrated']
        @numba.njit()
        def multiply_by_constant(vec, const, n):
            for i in range(n):
                vec[i] = vec[i]*const
            return vec
        position = multiply_by_constant(trace, c, len(trace))
        return position/1e-9 if nm else position