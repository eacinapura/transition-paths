# -*- coding: utf-8 -*-
import h5py
import numpy as np
import pickle as pkl
import sys

#====================================
# File paths
#====================================
filepath = "data/processed/"
file_idx = sys.argv[1]

input_file = filepath + 'data' + file_idx + ".h5"
output_file = filepath + 'outputs/potential2D/data' + file_idx + '.pkl'

#====================================
# Read data
#====================================
print("Reading file")

with h5py.File(input_file, mode='r') as reader:
    x = np.array(reader['main/x'])
    y = np.array(reader['main/y'])
    fs = reader['main'].attrs['fs']
T_sample = 1/fs
#====================================
# Create histogram of probabilities
#====================================
print("Calculating histogram")
num_bins = [100, 50]
hist, b_x, b_y = np.histogram2d(x, y, bins=num_bins, density=True, range=[(-0.23, 0.23), (-0.066, 0.066)])

#====================================
# Obtain potential
#====================================
print("Calculating potential")
pos_x = np.array([(b_x[i+1]+b_x[i])/2 for i in range(len(b_x)-1)])
pos_y = np.array([(b_y[i+1]+b_y[i])/2 for i in range(len(b_y)-1)])
U = -np.log(hist)  
U = U - np.min(U) 

#====================================
# Save output to file
#====================================
print('Saving output to file')
output = {
    'pos_x' : pos_x,
    'pos_y' : pos_y,
    'U' : U,
    'bins_x' : b_x,
    'bins_y' : b_y,
    'histogram' : hist
}

with open(output_file, 'wb') as f:
    pkl.dump(output, f)

print("Done!")
