from optomechanics.post_processing.read.gage_sig import OsciData
from python_routines import calibration
import numpy as np
import pandas as pd
import h5py
import sys
import numba
#=====================================
# Parameters
#=====================================
fs = 10e6       # in Hz
Ts = 1/fs

#=====================================
data_path = 'data/'
data_idx = sys.argv[1]

path_x = data_path + 'raw/data' + data_idx + '_CH01/Folder.00001/'
path_y = data_path + 'raw/data' + data_idx + '_CH02/Folder.00001/'

f_out = data_path + 'processed/data' + data_idx + '.h5'


#=====================================
# Combine traces
#=====================================
print('Combining traces')
num_files = 8   #number of files to be combined
x = np.array([])
y = np.array([])

for i in range(1, num_files+1):

    # print(f'Combining trace {i}/{num_files}', end='\r')

    f_x = path_x + f'data_CH01-{i:02d}.sig'
    f_y = path_y + f'data_CH02-{i:02d}.sig'
    x_osci = OsciData(f_x)
    y_osci = OsciData(f_y)
    x_part = x_osci.measured_data_raw
    y_part = y_osci.measured_data_raw

    n = len(x_part)

    X = pd.Series(x_part)
    rolling_time = 0.02
    rolling_num = int(rolling_time/Ts)
    ravg_X = X.rolling(rolling_num).max().to_numpy()
    ravg_X = ravg_X[rolling_num:]
    x_part = x_part[rolling_num//2 : n-rolling_num//2] - ravg_X

    max_x = np.max(x_part)
    min_x = np.min(x_part)

    x_part = x_part - (max_x + min_x)/2

    y_part = y_part[rolling_num//2 : n-rolling_num//2]

    y_part = y_part - np.average(y_part[:(len(y_part)//5)])

    x = np.append(x, x_part)
    y = np.append(y, y_part)

N = len(x)




#====================================
# Calibration
#====================================
print("Calibrating")
Calib_X = calibration.LinearCalibrator()
Calib_X.calibrate(x, fs = fs, particle_size=136/2*1e-9)
x = Calib_X.calibrate_trace(x)*1e6

Calib_Y = calibration.LinearCalibrator()
Calib_Y.calibrate(y, fs = fs, particle_size=136/2*1e-9)
y = Calib_Y.calibrate_trace(y)*1e6


# #====================================
# # Save to file
# #====================================
print("Saving data to file")
with h5py.File(f_out, mode='w') as writer:
        grp = writer.create_group('main')
        grp.attrs['fs'] = fs
        grp.create_dataset('x', data=x)
        grp.create_dataset('y', data=y)

print("Done!")