import h5py
import numpy as np
import pickle as pkl
import sys
from math import floor, sqrt
import numba

#====================================
# File paths
#====================================
filepath = "data/processed/"
file_idx = sys.argv[1]

input_timeseries = filepath + 'data' + file_idx + '.h5'
input_potential = filepath + "outputs/potential2D/data" + file_idx + ".pkl"
output_file = filepath + 'outputs/transitions/tr_indexes_data' + file_idx + '.pkl'

#====================================
# Read data
#====================================
# print("Reading data")

# Read time series
with h5py.File(input_timeseries, mode='r') as reader:
    x = np.array(reader['main/x'])
    y = np.array(reader['main/y'])
    fs = reader['main'].attrs['fs']
n = len(x)
Ts = 1/fs

# Read potential
with open(input_potential, 'rb') as f:
    data = pkl.load(f)

pos_x = data['pos_x']
pos_y = data['pos_y']
U = data['U']
b_x = data['bins_x']
b_y = data['bins_y']

X, Y = np.meshgrid(pos_x, pos_y)

#====================================
# Find transitions
#====================================
print('Finding transitions')

U0 = 1.0

@numba.njit()
def find_transitions(x, y, n, U, U0, pos_x, pos_y):
    trans = []
    direction = []
    dx = pos_x[1] - pos_x[0]
    dy = pos_y[1] - pos_y[0]
    for i in range(n):
        #===================
        # Indicator function
        #===================
        x_idx = floor((x[i]-pos_x[0])/dx)
        y_idx = floor((y[i]-pos_y[0])/dy)
        if U[x_idx][y_idx] < U0:
            if x[i] > 0:
                indicator = 1
            else:
                indicator = -1
        else:
            indicator = 0

        #===================
        # Transitions
        #===================
        if i == 0:
            prev_prev = indicator
            continue
        if i == 1:
            prev = indicator
            low_idx = 1 if prev == 0 else None
            continue
        if prev == 0 and indicator != 0: # re-entered in a trap
            if prev_prev != indicator:   # transition completed, save transition
                trans.append([low_idx,i])
                if prev_prev == 1 and indicator == -1:
                    direction.append('RL')
                else:
                    direction.append('LR')
        
        if prev != 0 and indicator == 0:     # exited from a trap
            low_idx = i

        if indicator != prev:     # in any case, update the states if state has changed
            prev_prev, prev = prev, indicator  
    return trans, direction

trans, direction = find_transitions(x, y, n, U, U0, pos_x, pos_y)


#====================================
# Save output to file
#====================================
print('Saving output to file')

output = {
    'trans' : trans,
    'direction' : direction
}

with open(output_file, 'wb') as f:
    pkl.dump(output, f)

print("Done!")

