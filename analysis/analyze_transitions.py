import pickle as pkl
import numpy as np
import h5py
from math import sqrt
from matplotlib import pyplot as plt
from matplotlib import rc, rcParams
from scipy.optimize import curve_fit
from scipy import constants
import sys

replot = 1
save_fig = 1
#====================================
# File paths
#====================================
data_path = 'data/'
data_path_processed = data_path + 'processed/'
data_path_raw = data_path + 'raw/'
file_idx = sys.argv[1]

input_timeseries = data_path_processed + 'data' + file_idx + '.h5'
input_potential = data_path_processed + 'outputs/potential2D/data' + file_idx + '.pkl'
input_transitions = data_path_processed + 'outputs/transitions/tr_indexes_data' + file_idx + '.pkl'
input_pressure = data_path_raw + 'pressures.txt'

output_averages_file = data_path_processed + 'outputs/transitions/averages_data' + file_idx + '.pkl'
output_transitions_file = data_path_processed + 'outputs/transitions/tr_extended_data' + file_idx + '.pkl'

figure_path = 'figures/'

#====================================
# Read data
#====================================
print("Reading data")
# Time series
with h5py.File(input_timeseries, mode='r') as reader:
    x = np.array(reader['main/x'])
    y = np.array(reader['main/y'])
    fs = reader['main'].attrs['fs']
n = len(x)
Ts = 1/fs

# Pressure
pressures = []
with open(input_pressure, 'r') as f:
    data = f.readlines()
    for line in data:
        pressures.append(float(line))

pressures = np.flip(np.array(pressures)) # I saved them in reversed order
pressure = pressures[int(file_idx)]

# Potential
with open(input_potential, 'rb') as f:
    data_pot = pkl.load(f)

pos_x = data_pot['pos_x']
pos_y = data_pot['pos_y']
b_x = data_pot['bins_x']
b_y = data_pot['bins_y']
U = data_pot['U']

X, Y = np.meshgrid(pos_x, pos_y)

# Transitions
with open(input_transitions, 'rb') as f:
    data_tr = pkl.load(f)

trans = data_tr['trans']
direction = data_tr['direction']

#==========================================
# Obtain the transitions from the raw data
#==========================================
print("Analyzing transitions")

use_threshold = False #set to true if you want to discard transitions longer than 'duration_threshold'
duration_threshold = 70e-6

# Duration of the transition
if use_threshold:
    durations = np.array([(idx[1]-idx[0])*Ts for idx in trans if (idx[1]-idx[0])*Ts < duration_threshold])
    dir = [di for di, idx in zip(direction, trans) if (idx[1]-idx[0])*Ts < duration_threshold]
    trans_x = [x[idx[0]:idx[1]] for idx in trans if (idx[1]-idx[0])*Ts < duration_threshold]
    trans_y = [y[idx[0]:idx[1]] for idx in trans if (idx[1]-idx[0])*Ts < duration_threshold]
else:
    durations = np.array([(idx[1]-idx[0])*Ts for idx in trans])
    trans_x = [x[idx[0]:idx[1]] for idx in trans]
    trans_y = [y[idx[0]:idx[1]] for idx in trans]

durations_LR = np.array([dur for dur, dir in zip(durations, direction) if dir=='LR'])
durations_RL = np.array([dur for dur, dir in zip(durations, direction) if dir=='RL'])
#---------------------------------------------
# Length of the transitions
lengths = []
for arr_x, arr_y, tr in zip(trans_x, trans_y, trans):
    l = 0
    for i in range(1, len(arr_x)):
        dl = sqrt((arr_x[i] - arr_x[i-1])**2 + (arr_y[i] - arr_y[i-1])**2)
        l += dl
    lengths.append(l)
lengths = np.array(lengths)
lengths_LR = np.array([dur for dur, dir in zip(lengths, direction) if dir=='LR'])
lengths_RL = np.array([dur for dur, dir in zip(lengths, direction) if dir=='RL'])


#=================================================
# Divide in batches and calculate average and std
#=================================================
num_batches = 10

# Durations
num_samples = len(durations)//num_batches
num_samples_LR = len(durations_LR)//num_batches
num_samples_RL = len(durations_RL)//num_batches

durations_batched = [durations[i*num_samples:(i+1)*num_samples] for i in range(num_batches)]
durations_RL_batched = [durations_LR[i*num_samples_LR:(i+1)*num_samples_LR] for i in range(num_batches)]
durations_LR_batched = [durations_RL[i*num_samples_RL:(i+1)*num_samples_RL] for i in range(num_batches)]

averages = np.array([np.average(arr) for arr in durations_batched])
averages_LR = np.array([np.average(arr) for arr in durations_LR_batched])
averages_RL = np.array([np.average(arr) for arr in durations_RL_batched])

avg_duration = np.average(averages)
avg_duration_LR = np.average(averages_LR)
avg_duration_RL = np.average(averages_RL)

std_duration = np.std(averages)
std_duration_LR = np.std(averages_LR)
std_duration_RL = np.std(averages_RL)


#---------------------------------------------
# Lengths
num_samples = len(lengths)//num_batches
num_samples_LR = len(lengths_LR)//num_batches
num_samples_RL = len(lengths_RL)//num_batches

lengths_batched = [lengths[i*num_samples:(i+1)*num_samples] for i in range(num_batches)]
lengths_RL_batched = [lengths_LR[i*num_samples_LR:(i+1)*num_samples_LR] for i in range(num_batches)]
lengths_LR_batched = [lengths_RL[i*num_samples_RL:(i+1)*num_samples_RL] for i in range(num_batches)]

averages = np.array([np.average(arr) for arr in lengths_batched])
averages_LR = np.array([np.average(arr) for arr in lengths_LR_batched])
averages_RL = np.array([np.average(arr) for arr in lengths_RL_batched])

avg_length = np.average(averages)
avg_length_LR = np.average(averages_LR)
avg_length_RL = np.average(averages_RL)

std_length = np.std(averages)
std_length_LR = np.std(averages_LR)
std_length_RL = np.std(averages_RL)




#=================================================
# Linear regression length-duration
#=================================================
def f(x, m, q):
    return m*x + q
estimate_m = 1.5/40
fit_output = curve_fit(f, durations*1e6, lengths, [estimate_m, 0])
popt, pcov = fit_output
m = popt[0]
q = popt[1]
err_m = np.sqrt(np.diag(pcov))[0]

particle_size = 136e-9 / 2
rho_SiO2 = 2200
particle_mass = 4 * np.pi * rho_SiO2 * particle_size ** 3 / 3
T = 25 + 273
expected_velocity = sqrt(constants.Boltzmann*T / particle_mass)

d_points = np.linspace(min(durations), max(durations), 10)
l_points = np.linspace(min(lengths), max(lengths), 10)

print(f"Experimental velocity: {m:1.4f} +- {err_m:1.4f} m/s")
print(f"Theoretical velocity: {expected_velocity:1.4f} m/s")

#====================================
# Save output to file
#====================================
output_transitions = {
    'durations' : durations,
    'lenghts' : lengths,

    'direction' : direction,
}

output_avg = {
    'avg_duration' : avg_duration,
    'avg_duration_LR' : avg_duration_LR,
    'avg_duration_RL' : avg_duration_RL,

    'std_duration' : std_duration,
    'std_duration_LR' : std_duration_LR,
    'std_duration_RL' : std_duration_RL,

    'avg_length' : avg_length,
    'avg_length_LR' : avg_length_LR,
    'avg_length_RL' : avg_length_RL,

    'std_length' : std_length,
    'std_length_LR' : std_length_LR,
    'std_length_RL' : std_length_RL,
}

with open(output_averages_file, 'wb') as f:
    pkl.dump(output_avg, f)

with open(output_transitions_file, 'wb') as f:
    pkl.dump(output_transitions, f)

if not replot:
    exit(0)

#======================================================================================================
# Plots
#======================================================================================================
print('Plotting')
suffixes = ['all', 'LR', 'RL']
#====================================
# Matplotlib settings
#====================================
fontsize = 11
# rcParams['font.family'] = 'Tex Gyre Heros'
rcParams['font.family'] = 'Serif'
rcParams['text.usetex'] = True
rcParams['font.size'] = fontsize
rc('xtick', labelsize=fontsize)
rc('ytick', labelsize=fontsize)
cm = 1/2.54

'''
If you want to make the plots distinguishing the direction of the transitions, uncomment the first for loop and comment the second one
'''
# for suffix, dur, le in zip(suffixes, [durations, durations_LR, durations_RL], [lengths, lengths_LR, lengths_RL]):
for suffix, dur, le in zip(suffixes, [durations], [lengths]):
    #=========================================
    # Plot - Histogram of durations and length
    #=========================================
    # fig = plt.figure(figsize = (16*cm, 8*cm))
    fig = plt.figure(figsize = (13*cm, 6*cm))
    plt.subplot(1, 2, 1)
    # plt.title("Duration of the transitions - " + suffix + ' - ' + f'{pressure} mbar')
    plt.hist(dur*1e6, bins=40, density=1, range=(0, duration_threshold*1e6), color='firebrick')
    plt.xlabel(r'Duration [$\mu$s]')
    plt.ylabel('Probability density')
    
    plt.subplot(1, 2, 2)
    # plt.title("Length of the transitions - " + suffix + ' - ' + f'{pressure} mbar')
    plt.hist(le, bins=40, density=1, color='royalblue')
    plt.xlabel(r'Length [$\mu$m]')
    plt.ylabel('Probability density')

    fig.subplots_adjust(right=0.95, wspace=0.5)
    plt.tight_layout()
    # plt.savefig(figure_path + 'histograms_length/hist_length_data' + file_idx + '_' + suffix + '.png')
    plt.savefig(figure_path + 'histograms/histogram_' + file_idx +'.pdf')
    plt.close()

    #====================================
    # Plot - Correlation
    #====================================
    plt.figure(figsize=(7*cm, 7*cm))
    # plt.title("Duration vs length - " + suffix + ' - ' + f'{pressure} mbar')
    plt.plot(dur*1e6, le, '.', markersize = 4, color = 'k', label='Data')
    plt.plot(d_points*1e6, m*d_points*1e6 + q, '-', color = 'r', label='Fit')
    # plt.text(5, 2.5, r"slope:", color='r')
    # plt.text(5, 2.3, r"0.0382 $\pm$ 0.0001 m/s", color='r')
    plt.xlabel(r'Duration [$\mu$s]')
    plt.ylabel(r'Length [$\mu$m]')
    plt.legend()
    plt.tight_layout()
    plt.savefig(figure_path + 'correlation/correlation_' + file_idx + '_' + suffix + '.pdf')

    # plt.show()
    