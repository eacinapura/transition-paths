import sys
import numpy as np
import pickle as pkl
from matplotlib import pyplot as plt
from matplotlib import rc, rcParams

#==========================================
# Data paths
#==========================================

file_range = range(int(sys.argv[1]), int(sys.argv[2]) + 1)

data_path = 'data/'

input_averages = [data_path + 'processed/outputs/transitions/averages_' + f'data{i}.pkl' for i in file_range]
input_pressure = data_path + 'raw/pressures.txt'
figure_path = 'figures/averages/'

#==========================================
# Read data
#==========================================
# Duration and length
duration = []
duration_LR = []
duration_RL = []

std_duration = []
std_duration_LR = []
std_duration_RL = []

length = []
length_LR = []
length_RL = []

std_length = []
std_length_LR = []
std_length_RL = []

for file in input_averages:
    with open(file, 'rb') as f:
        data = pkl.load(f)

        duration.append(data['avg_duration'])
        duration_LR.append(data['avg_duration_LR'])
        duration_RL.append(data['avg_duration_RL'])

        length.append(data['avg_length'])
        length_LR.append(data['avg_length_LR'])
        length_RL.append(data['avg_length_RL'])
        
        std_duration.append(data['std_duration'])
        std_duration_LR.append(data['std_duration_LR'])
        std_duration_RL.append(data['std_duration_RL'])

        std_length.append(data['std_length'])
        std_length_LR.append(data['std_length_LR'])
        std_length_RL.append(data['std_length_RL'])

# Pressure
pressure = []
with open(input_pressure, 'r') as f:
    data = f.readlines()
    for line in data:
        pressure.append(float(line))

pressure = np.flip(np.array(pressure)) # I saved them in reversed order
# Transform in numpy arrays
# for arr in [duration, duration_LR, duration_RL, length, length_LR, length_RL, std_duration, std_duration_LR, std_duration_RL, std_length, std_length_LR, std_length_RL]:
#     arr = np.array(arr)


#======================================================================================================
# Plots
#======================================================================================================
print('Plotting')
suffixes = ['all', 'LR', 'RL']
#====================================
# Matplotlib settings
#====================================
fontsize = 8
rcParams['font.family'] = 'Tex Gyre Heros'
# rcParams['font.family'] = 'Serif'
# rcParams['text.usetex'] = True
rcParams['font.size'] = fontsize
rc('xtick', labelsize=fontsize)
rc('ytick', labelsize=fontsize)
cm = 1/2.54


for suffix, dur, std_dur, le, std_le in zip(suffixes, [duration], [std_duration], [length], [std_length]):
# for suffix, dur, std_dur, le, std_le in zip(suffixes, [duration, duration_LR, duration_RL], [std_duration, std_duration_LR, std_duration_RL], [length, length_LR, length_RL], [std_length, std_length_LR, std_length_RL]):
    #======================
    # Plot duration
    #======================
    # fig = plt.figure(figsize=(16*cm, 8*cm))
    fig = plt.figure(figsize=(13*cm, 6*cm))

    plt.subplot(1, 2, 1)
    plt.errorbar(pressure, np.array(dur)*1e6, yerr=np.array(std_dur)*1e6, marker='.', markersize=10, color = 'firebrick')
    plt.xscale('log')
    # plt.title('Duration of the transitions vs pressure - ' + suffix)
    plt.xlabel('Pressure [mbar]')
    plt.ylabel(r'Duration [$\mu$s]')
    # plt.savefig(figure_path + 'duration.pdf')
    # plt.savefig(figure_path + 'duration_' + suffix + '.pdf')

    plt.subplot(1, 2, 2)
    plt.errorbar(pressure, np.array(le), yerr=np.array(std_le), marker='.', markersize=10, color='royalblue')
    plt.xscale('log')

    # plt.title('Length of the transitions vs pressure - ' + suffix)
    plt.xlabel('Pressure [mbar]')
    plt.ylabel(r'Length [$\mu$m]')
    # plt.savefig(figure_path + 'length_no_threshold.pdf')
    fig.subplots_adjust(right=0.95, wspace=0.5)
    plt.tight_layout()

    # plt.savefig(figure_path + 'averages_' + suffix + '.pdf')
    plt.savefig(figure_path + 'averages.pdf')
    plt.show()



# #==========================================
# # Scatter plot
# #==========================================
# plt.figure('scatter distance-length')
# plt.title("Duration-length correlation")
# plt.scatter(duration*1e6, length, color='k')

# plt.xlabel(r'Duration [$\mu$s]')
# plt.ylabel(r'Length [$\mu$m]')
# plt.savefig(figure_path + 'correlation_no_threshold.pdf')
# plt.savefig(figure_path + 'correlation_no_threshold.png')
# plt.close()
