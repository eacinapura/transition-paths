import pickle as pkl
import numpy as np
import h5py
from math import sqrt
from matplotlib import pyplot as plt
import matplotlib as mpl
from matplotlib.animation import FuncAnimation
import matplotlib.colors as mcolors
import sys

U0 = 1.0
#====================================
# File paths
#====================================
data_path = 'data/processed/'
file_idx = sys.argv[1]

input_timeseries = data_path + 'data' + file_idx + '.h5'
input_potential = data_path + 'outputs/potential2D/data' + file_idx + '.pkl'
input_transitions = data_path + 'outputs/transitions/tr_indexes_data' + file_idx + '.pkl'
input_durations_lengths = data_path + 'outputs/transitions/tr_extended_data' + file_idx + '.pkl'
input_averages = data_path + 'outputs/transitions/averages_data' + file_idx + '.pkl'

figure_path = 'figures/'

#====================================
# Read data
#====================================
print("Reading data")
# Time series
with h5py.File(input_timeseries, mode='r') as reader:
    x = np.array(reader['main/x'])
    y = np.array(reader['main/y'])
    fs = reader['main'].attrs['fs']
n = len(x)
Ts = 1/fs
# times = np.linspace(0, n*Ts, n)

# Potential
with open(input_potential, 'rb') as f:
    data_pot = pkl.load(f)

pos_x = data_pot['pos_x']
pos_y = data_pot['pos_y']
b_x = data_pot['bins_x']
b_y = data_pot['bins_y']
U = data_pot['U']

X, Y = np.meshgrid(pos_x, pos_y)


# Transitions
with open(input_transitions, 'rb') as f:
    data_tr = pkl.load(f)

with open(input_durations_lengths, 'rb') as f:
    data_dur = pkl.load(f)

trans = data_tr['trans']
durations = data_dur['durations']

init_time = [t[0] for t in trans]


# Averages
with open(input_averages, 'rb') as f:
    data = pkl.load(f)

    duration = data['avg_duration']
    std_duration = data['std_duration']
        
trans_x = [x[idx[0]:idx[1]] for idx in trans]
trans_y = [y[idx[0]:idx[1]] for idx in trans]

hist_trans, bins_trans_x, bins_trans_y = np.histogram2d(np.concatenate(trans_x), np.concatenate(trans_y), bins=[b_x, b_y], density=True)


trans_x_anomalous = [t for t,dur in zip(trans_x, durations) if 40e-6 < dur < 300e-6]
trans_y_anomalous = [t for t,dur in zip(trans_y, durations) if 40e-6 < dur < 300e-6]
trans_times_anomalous = [t for t, dur in zip(trans, durations) if 40e-6 < dur < 300e-6] 

# for t in trans_times_anomalous:
#     print(t[0]*Ts)

#====================================
# Do statistics on the data
#====================================
print("Analyzing transitions")

# Histogram
# hist_trans, bins_trans_x, bins_trans_y = np.histogram2d(np.concatenate(trans_x), np.concatenate(trans_y), bins=[b_x, b_y], density=True)

#====================================
# Matplotlib settings
print("Plotting")
#====================================
fontsize = 11
# mpl.rcParams['font.family'] = 'Tex Gyre Heros'
mpl.rcParams['font.family'] = 'Serif'
mpl.rcParams['text.usetex'] = True
mpl.rcParams['font.size'] = fontsize
mpl.rc('xtick', labelsize=fontsize)
mpl.rc('ytick', labelsize=fontsize)
cm = 1/2.54

#====================================
# Plot - Transitions in time
#====================================
if 0:
    plt.figure()
    plt.title(f'Data {file_idx}')
    plt.plot(np.concatenate(trans_times_anomalous)*1e3, np.concatenate(trans_x_anomalous), '.')
    plt.ylabel(r'Duration [$\mu$s]')
    plt.show()

#====================================
# Plot - Durations
#====================================
if 0:
    plt.figure()
    plt.title(r'With duration threshold of 70 $\mu$s')
    plt.plot(durations*1e6, '.', c='k')
    # plt.hlines(duration*1e6, 0, len(durations), color='r')
    plt.xlabel('Time [arb.]')
    plt.ylabel(r'Duration [$\mu$s]')
    plt.show()


#====================================
# Plot - Animation
#====================================
if 0:
    fig_trans = plt.figure('trajectory', figsize=(24*cm, 15*cm))

    plt.pcolormesh(pos_x, pos_y, np.transpose(U), shading='nearest')
    plt.colorbar()
    plt.title("Trajectories of the transitions")
    plt.xlabel(r"$x$ [$\mu$m]")
    plt.ylabel(r"$y$ [$\mu$m]")
    C = plt.contour(X, Y, np.transpose(U), [U0], colors='red')
    plt.clabel(C, inline=True, fmt=lambda s: rf"{s:1.0f} $k_B T$", fontsize=8)


    line, = plt.plot([],[], '-', marker='.', markersize=3, color='red')

    def animate(i):
        line.set_data(trans_x[i+1], trans_y[i+1])
        return line,

    anim = FuncAnimation(fig_trans, animate,frames=30, interval=1000, blit=True)

    anim.save(figure_path + 'animation/data' + file_idx + '.gif')
    plt.show()

#====================================
# Plot - Histogram on potential
#====================================
if 0:
    # Define colomap for histogram values
    colors = [(1,0,0,c) for c in np.linspace(0,1,100)]
    cmapred = mcolors.LinearSegmentedColormap.from_list('mycmap', colors, N=200)

    fig_heat = plt.figure('trajectory', figsize=(13*cm, 8*cm))

    hm1 = plt.pcolormesh(pos_x, pos_y, np.transpose(U), shading='nearest', rasterized=True)
    hm2 = plt.pcolormesh(pos_x, pos_y, np.transpose(hist_trans), shading='nearest', cmap=cmapred, rasterized=True)
    C = plt.contour(X, Y, np.transpose(U), [U0], colors='red')

    cbar2 = plt.colorbar(hm2)
    cbar1 = plt.colorbar(hm1)

    cbar1.set_label(r'U [$k_B$ T]')
    cbar2.set_label(r'Transition probability density [$\mu$m$^{-2}$]')
    plt.xlabel(r"$x$ [$\mu$m]")
    plt.ylabel(r"$y$ [$\mu$m]")
    plt.clabel(C, inline=True, fmt=lambda s: rf"{s:1.0f} $k_B T$", fontsize=8)


    plt.tight_layout()
    plt.savefig(figure_path + 'paths_heatmap/transition_prob_' + file_idx + '.pdf')
    # plt.show()

#====================================
# Plot - Some transitions
#====================================

# fig, axes = plt.subplots(nrows = 2, ncols = 3, sharex = True, sharey=True, figsize=(16*cm, 10*cm))
fig, axes = plt.subplots(nrows = 2, ncols = 3, sharex = True, sharey=True, figsize=(13*cm, 7*cm))
for i, ax in enumerate(axes.flat):
    hm = ax.pcolormesh(pos_x, pos_y, np.transpose(U), shading='nearest', rasterized=True)
    ax.plot(trans_x[i],trans_y[i], '-', linewidth=1, color='r')
    ax.set(xlabel=r"$x$ [$\mu$m]", ylabel=r"$y$ [$\mu$m]")
    C = ax.contour(X, Y, np.transpose(U), [U0], colors='red', linestyles='dotted')
    
fig.subplots_adjust(right=0.85)
fig.subplots_adjust(top=0.95)
fig.subplots_adjust(bottom=0.15)
cbar_ax = fig.add_axes([0.89, 0.15, 0.02, 0.7])
cbar = fig.colorbar(hm, cax=cbar_ax)
cbar.set_label(r'U [$k_B$ T]')

for ax in axes.flat:
    ax.label_outer()

plt.savefig(figure_path + 'transitions/transitions_' + file_idx + '.pdf')
# plt.show()




