import pickle as pkl
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc, rcParams
from mpl_toolkits.mplot3d import axes3d
import sys
from math import floor

save_fig = 1
showfig = 0
#====================================
# File paths
#====================================
data_path = 'data/processed/outputs/'
file_idx = sys.argv[1]

filename = data_path + 'potential2D/' + file_idx + '.pkl'
figure_path = 'figures/potential2D/'

#====================================
# Read data
#====================================
with open(filename, 'rb') as f:
    data = pkl.load(f)

pos_x = data['pos_x']
pos_y = data['pos_y']
U = data['U']

X, Y = np.meshgrid(pos_x, pos_y)
U0 = [1]
#====================================
# Matplotlib settings
#====================================
fontsize = 8
rcParams['font.family'] = 'Tex Gyre Heros'
# rcParams['font.family'] = 'Serif'
# rcParams['text.usetex'] = True
rcParams['font.size'] = fontsize
rc('xtick', labelsize=fontsize)
rc('ytick', labelsize=fontsize)
cm = 1/2.54

#====================================
# Plot - Heatmap
#====================================
# plt.figure(f"Potential2D - heatmap", figsize=(12*cm, 8*cm))
plt.figure(f"Potential2D - heatmap", figsize=(10*cm, 6*cm))

# plt.title('Potential')
plt.pcolormesh(pos_x, pos_y, np.transpose(U), shading='nearest', rasterized=True)
plt.xlabel(r"$x$ [$\mu$m]")
plt.ylabel(r"$y$ [$\mu$m]")
cbar = plt.colorbar()
cbar.set_label(r'U [$k_BT$]')
C = plt.contour(X, Y, np.transpose(U), U0, colors='red')
plt.clabel(C, inline=True, fmt=lambda s: rf"{s:1.0f} $k_B T$", fontsize=8)

plt.tight_layout()
if save_fig:
    plt.savefig(figure_path + 'potential2D_' + file_idx + '.pdf')
if showfig:
    plt.show()
else:
    plt.close()

#====================================
# Plot - Cross sections
#====================================
# fig = plt.figure(figsize = (16*cm, 8*cm))
fig = plt.figure(figsize = (12*cm, 6*cm))

dx = pos_x[1] - pos_x[0]
dy = pos_y[1] - pos_y[0]
x0_idx = floor((-pos_x[0])/dx)
y0_idx = floor((-pos_y[0])/dy)

plt.subplot(1, 2, 1)
plt.plot(pos_x, U[:, y0_idx], color='k')
plt.xlabel(r"$x$ [$\mu$m]")
plt.ylabel(r'U [$k_BT$]')

plt.subplot(1, 2, 2)
plt.plot(pos_y, U[x0_idx, :], color='k')
plt.xlabel(r"$y$ [$\mu$m]")

fig.subplots_adjust(right=0.95, wspace=0.5)
plt.tight_layout()
plt.savefig(figure_path + 'cross_sections_' + file_idx + '.pdf')
plt.close()

# #====================================
# # Plot - 3D representation
# #====================================
# fig = plt.figure(f"Potential 2D - 3D plot", figsize=(14*cm, 14*cm))

# ax = plt.axes(projection='3d')
# ax.plot_surface(X, Y, np.transpose(U), cmap='viridis')
# plt.xlabel(r"x [$\mu$m]")
# plt.ylabel(r"y [$\mu$m]")

# plt.tight_layout()
# if save_fig:
#     plt.savefig(figure_path + f'/pdf/3Dplot_' + file_idx + '.pdf')
#     plt.savefig(figure_path + f'/png/3Dplot_' + file_idx + '.png')
# plt.close()

