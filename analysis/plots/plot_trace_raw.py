from optomechanics.post_processing.read.gage_sig import OsciData
import numpy as np
import pandas as pd
import h5py
import sys
import numba
from matplotlib import pyplot as plt
import matplotlib as mpl
#====================================
# Matplotlib settings
#====================================
fontsize = 9
# mpl.rcParams['font.family'] = 'Tex Gyre Heros'
mpl.rcParams['font.family'] = 'Serif'
mpl.rcParams['text.usetex'] = True
mpl.rcParams['font.size'] = fontsize
mpl.rc('xtick', labelsize=fontsize)
mpl.rc('ytick', labelsize=fontsize)
cm = 1/2.54
#=====================================
# Parameters
#=====================================
fs = 10e6       # in Hz
Ts = 1/fs
cut_down_factor = 100
fs_reduced = fs/cut_down_factor
Ts_reduced = 1/fs_reduced

#=====================================
data_path = 'data/'
data_idx = sys.argv[1]

path_x = data_path + 'raw/data' + data_idx + '_CH01/Folder.00001/'
path_y = data_path + 'raw/data' + data_idx + '_CH02/Folder.00001/'

input_pressure = data_path + 'raw/pressures.txt'

f_out = data_path + 'processed/' + data_idx + '.h5'

figure_path = 'figures/timeseries/'

num_files = 8

#=====================================
# Combine traces
#=====================================
x = np.array([])
y = np.array([])
x_reduced = np.array([])
y_reduced = np.array([])

for i in range(1, num_files+1):

    print(f'Combining trace {i}/{num_files}', end='\r')

    f_x = path_x + f'data_CH01-{i:02d}.sig'
    f_y = path_y + f'data_CH02-{i:02d}.sig'
    x_osci = OsciData(f_x)
    y_osci = OsciData(f_y)
    x_part = x_osci.measured_data_raw
    y_part = y_osci.measured_data_raw
    x = np.append(x, x_part)
    y = np.append(y, y_part)
    x_reduced = np.append(x_reduced, x_part[::cut_down_factor])
    y_reduced = np.append(y_reduced, y_part[::cut_down_factor])
n = len(x)
print('\n')

# Pressure
pressures = []
with open(input_pressure, 'r') as f:
    data = f.readlines()
    for line in data:
        pressures.append(float(line))

pressures = np.flip(np.array(pressures)) # I saved them in reversed order
pressure = pressures[int(data_idx)]

#====================================
# Plot - 50 ms
#====================================
time_low = 0
time_high = 50e-3
idx_low = int(time_low/Ts_reduced)
idx_high = int(time_high/Ts_reduced)

t_reduced = np.arange(idx_low, idx_high)*Ts_reduced

slice_reduced = np.s_[idx_low:idx_high]


print('Plotting')

fig = plt.figure('timeseries', figsize=(10*cm, 7*cm))
# fig.suptitle(f"Raw time traces - {pressure} mbar", fontsize=12)

plt.subplot(2, 1, 1)
plt.plot(t_reduced*1e3, x_reduced[slice_reduced], '.', color='k', markersize=2)
plt.ylabel(r"$x$ position [bits]")

plt.subplot(2, 1, 2)
plt.plot(t_reduced*1e3, y_reduced[slice_reduced], '.', color='k', markersize=2)
plt.ylabel(r"$y$ position [bits]")

plt.xlabel("Time [ms]")

plt.tight_layout()
plt.savefig(figure_path + 'timeseries_raw_50ms_' + data_idx + '.pdf')   
# plt.close()
# plt.show()


#====================================
# Plot - 1 s
#====================================
time_low = 0
time_high = 1
idx_low = int(time_low/Ts_reduced)
idx_high = int(time_high/Ts_reduced)

t_reduced = np.arange(idx_low, idx_high)*Ts_reduced

slice_reduced = np.s_[idx_low:idx_high]

fig = plt.figure('timeseries', figsize=(13*cm, 8*cm))
# fig.suptitle(f"Raw time traces - {pressure} mbar", fontsize=12)

plt.subplot(2, 1, 1)
plt.plot(t_reduced, x_reduced[slice_reduced], '.', color='k', markersize=2)
plt.ylabel(r"$x$ position [bits]")

plt.subplot(2, 1, 2)
plt.plot(t_reduced, y_reduced[slice_reduced], '.', color='k', markersize=2)
plt.ylabel(r"$y$ position [bits]")

plt.xlabel("Time [s]")

plt.tight_layout()
# plt.show()
plt.savefig(figure_path + 'timeseries_raw_1s_' + data_idx + '.pdf')   
