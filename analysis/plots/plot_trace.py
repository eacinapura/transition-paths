# -*- coding: utf-8 -*-
import h5py
import sys
import numpy as np
from matplotlib import pyplot as plt
import matplotlib as mpl
from math import floor

#====================================
# File paths
#====================================
filepath = "data/processed/"
file_idx = sys.argv[1]

input_file = filepath + 'data' + file_idx + ".h5"
input_transitions = filepath + 'outputs/transitions/tr_extended_data' + file_idx + '.pkl'
figure_path = 'figures/timeseries/'

#====================================
# Read data
#====================================
print("Reading file")
reduce_factor = 100

with h5py.File(input_file, mode='r') as reader:
    x = np.array(reader['main/x'])[::reduce_factor]
    y = np.array(reader['main/y'])[::reduce_factor]
    fs = reader['main'].attrs['fs']
Ts = 1/fs
n = len(x)


Ts_reduced = Ts*reduce_factor

#====================================
# Plotting parameters
#====================================
print('Plotting')
time_low = 0
time_high = 1
idx_low = int(time_low/Ts_reduced)
idx_high = int(time_high/Ts_reduced)

t_reduced = np.arange(idx_low, idx_high)*Ts_reduced

slice_reduced = np.s_[idx_low:idx_high]

#====================================
# Matplotlib settings
#====================================
fontsize = 11
# mpl.rcParams['font.family'] = 'Tex Gyre Heros'
mpl.rcParams['font.family'] = 'Serif'
mpl.rcParams['text.usetex'] = True
mpl.rcParams['font.size'] = fontsize
mpl.rc('xtick', labelsize=fontsize)
mpl.rc('ytick', labelsize=fontsize)
cm = 1/2.54
#====================================
# Plot
#====================================
fig = plt.figure('timeseries', figsize=(13*cm, 8*cm))
plt.subplot(2,1,1)
plt.plot(t_reduced*1e3, x[slice_reduced], '.', color='k', markersize=2)
# plt.ylim(-0.2, 0.2)
plt.ylabel(r"$x$ position [$\mu$m]")
plt.subplot(2,1,2)
plt.plot(t_reduced*1e3, y[idx_low:idx_high], '.', color='k', markersize=2)
plt.ylabel(r"$y$ position [$\mu$m]")
plt.xlabel("Time [ms]")


plt.tight_layout()
plt.savefig(figure_path + f'trace_processed_' + file_idx + '.pdf')
# plt.show()   
plt.close() 



print("\nDone!")
