# Transition paths in a bistable potential

Source code for the analysis of the transitions paths of a levitated nanoparticle in a bistable potential.

The indications below might not be complete or fully understandable. For further information, contact me to eacinapura@ethz.ch

## Content of the repo
The analysis scripts for this repo are contained in the folder `analysis`. Here is a list of the programs and their functionality:
- `preprocessing.py`: read raw data, filter it if needed, calibrate the traces and save them in h5 format
- `potential2D.py`: calculate the 2D potential from the calibrated time traces and save it
- `get_transitions.py`: read calibrated traces, potential data find the transitions, and save their initial and final time instants
- `analyze_transitions.py`: analyze durations and lengths of the transitions

Moreover, in the subdirectory `analysis/plots` there are some scripts to make some plots.

## How to run the source code
### Generate repo structure
The code in this repo assumes that the raw data is saved in the a folder `data/raw`. 
The first thing to do before running the code is to execute the script `prepare_directory.sh`, which creates the directories needed by the scripts to store partial outputs and the figures

### Run the analysis
We advise to run the analysis programs via the bash scripts prepare for this purpose. Each script accepts two parameter from the command line, representing the lower and higher indices of the files to be analyzed. Suppose for instance that you have collected time traces for 20 pressures: the indexes of the files will range from 0 to 19 (extrema included). We advise to run these scripts in the following order (example assuming that 20 files have to be processed):
```
./preprocessing.sh 0 19
./plot_trace_raw.sh 0 19
.plot_trace.sh 0 19
./potential_2D.sh 0 19
./get_transitions.sh 0 19
./analyze_transitions.sh 0 19
./plot_transitions.sh 0 19
./averages.sh 0 19
./plot_transitions.sh 0 19
```